//
//  Array.cpp
//  CommandLineTool
//
//  Created by Andrew Warren on 09/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#include "Array.h"
#include <iostream>


Array::Array()
{
    arraySize = 0;
    data = new float[arraySize];
}

Array::~Array()
{
    delete[] data;
}

void Array::add (float itemValue)
{
    float* tempData = new float[arraySize+1];
    
    for (int count = 0; count <=arraySize; count++)
    {
        tempData[count] = data[count];
    }
    tempData[arraySize+1] = itemValue;
    delete [] data;
    data = tempData;
    arraySize++;
}

float Array::get (int index)
{
    if (index >= 0 && index < size()) {
        return data[index];
    }
    else
    return 0.f;
    
}

int Array::size ()
{
    return arraySize;
}

bool testArray()
{
    Array array;
    float tempArray[] = {0., 1., 2., 3., 4.};
    int tempArraySize = sizeof(tempArray);
    
    if(array.size() != 0)
    {
        std::cout << "size is incorrect " << std::endl;
        return false;
    }
    
    for(int i = 0; i < tempArraySize; i++)
    {
        array.add(tempArray[i]);
        
        if(array.size() != i +1)
        {
            std::cout << "size doesnt increase";
            return false;
        }
        
        if(array.get(i) != tempArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
  
    
    return true;
}

