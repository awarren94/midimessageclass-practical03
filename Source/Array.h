//
//  Array.h
//  CommandLineTool
//
//  Created by Andrew Warren on 09/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <stdio.h>

class Array
{
public:
    Array();
    
    ~Array();
    
    void add (float itemValue);
    
    float get (int index);
    
    int size ();
    
private:
    
    int arraySize;
    float* data = nullptr;
};

bool testArray();

#endif /* defined(__CommandLineTool__Array__) */
