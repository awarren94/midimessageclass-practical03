//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Andrew Warren on 14/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef H_LINKEDLIST
#define H_LINKEDLIST

#include <stdio.h>

class LinkedList
{
public:
    /* a constructor that initializes the head to point to nullptr */
    LinkedList();
  
    /* a destructor that goes through and deletes all nodes in the list */
    ~LinkedList();
   
    /* which adds new items to the end of the array */
    void add (float itemVaue);
   
    /* which returns the value stored in the Node at the specified index */
    float get (int index);
   
    /* which returns the number of items currently in the linked list */
    int size();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    
  //  typedef struct Node* nodePtr;
    
    Node* head;
    Node* temp;
    Node* current;
};

#endif /* defined(H_LINKEDLIST) */
